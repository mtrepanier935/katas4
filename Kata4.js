const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1() {
    let header = document.createElement("div");
    header.textContent = "Kata 1";
    document.body.appendChild(header);

    test1();
}
function test1() {

    gotCitiesArray = gotCitiesCSV.split(",");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(gotCitiesArray);
    document.body.appendChild(newElement)
    return gotCitiesArray; // Don't forget to return your output!
}

function kata2() {
    let header = document.createElement("div");
    header.textContent = "Kata 2";
    document.body.appendChild(header);

    test2();
}
function test2() {

    bestThingArray = bestThing.split(" ");
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThingArray);
    document.body.appendChild(newElement)
    return bestThingArray; // Don't forget to return your output!
}

function kata3() {
    let header = document.createElement("div");
    header.textContent = "Kata 3";
    document.body.appendChild(header);

    test3();
}
function test3() {
    let newElement = document.createElement("div");
    newElement.textContent = gotCitiesCSV.split(",").join(";");
    document.body.appendChild(newElement)
    return gotCitiesCSV.split(",").join(";"); // Don't forget to return your output!
}
function kata4() {
    let header = document.createElement("div");
    header.textContent = "Kata4";
    document.body.appendChild(header);

    test4();
}
function test4() {
    let newElement = document.createElement("div");
    newElement.textContent = lotrCitiesArray.join(",");
    document.body.appendChild(newElement)
    return lotrCitiesArray.join(","); // Don't forget to return your output!
}
function kata5() {
    let header = document.createElement("div");
    header.textContent = "Kata5";
    document.body.appendChild(header);

    test5();
}
function test5() {
    let MyArray = []
    for (i = 0; i < lotrCitiesArray.length; i++)
        if (i < 5) {
            MyArray.push(lotrCitiesArray[i]);
        }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(MyArray);
    document.body.appendChild(newElement)
    return MyArray; // Don't forget to return your output!
}
function kata6() {
    let header = document.createElement("div");
    header.textContent = "Kata6";
    document.body.appendChild(header);

    test6();
}
function test6() {
    let MyArray = []
    for (i = 0; i < lotrCitiesArray.length; i++)
        if (i > 5) {
            MyArray.push(lotrCitiesArray[i]);
            lotrCitiesArray.length;
        }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray.slice(-5));
    document.body.appendChild(newElement)
    console.log(lotrCitiesArray.slice(-5)); // Don't forget to return your output!
}

function kata7() {
    let header = document.createElement("div");
    header.textContent = "Kata7";
    document.body.appendChild(header);

    test7();
}
function test7() {
    let MyArray = []
    for (i = 0; i < lotrCitiesArray.length; i++)
        if (i > 1 && i < 5) {
            MyArray.push(lotrCitiesArray[i]);
        }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(MyArray);
    document.body.appendChild(newElement)
    return MyArray; // Don't forget to return your output!
}
function kata8() {
    let header = document.createElement("div");
    header.textContent = "Kata8";
    document.body.appendChild(header);

    test8();
}
function test8() {
    for (i = 0; i < lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i] == "Rohan") {
            lotrCitiesArray.splice(i, 1);
            break;
        }
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; // Don't forget to return your output!
}

function kata9() {
    let header = document.createElement("div");
    header.textContent = "Kata9";
    document.body.appendChild(header);

    test9();
}
function test9() {
            lotrCitiesArray.splice(5,2);
                
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
}
function kata10() {
    let header = document.createElement("div");
    header.textContent = "Kata10";
    document.body.appendChild(header);

    test10();
}
function test10() {
    for (i = 0; i < lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i] == "Rohan") {
            i = -1;
            break;
        }
    }
    if (i != -1) {
        lotrCitiesArray.splice(2, 0, "Rohan");
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
} function kata11() {
    let header = document.createElement("div");
    header.textContent = "Kata11";
    document.body.appendChild(header);

    test11();
}
function test11() {
    for (i = 0; i < lotrCitiesArray.length; i++) {
        if (lotrCitiesArray[i] == "Dead Marshes") {
            lotrCitiesArray.splice(i, 1, "Deadest Marshes");
            break;
        }
    }
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
}
function kata12() {
    let header = document.createElement("div");
    header.textContent = "Kata 12";
    document.body.appendChild(header);

    test12();
}
function test12() {

    bestThingArray = bestThing.slice(0, 14);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThing.slice(0, 14));
    document.body.appendChild(newElement)
    return bestThing.slice(0, 14); // Don't forget to return your output! //off by a character
}

function kata13() {
    let header = document.createElement("div");
    header.textContent = "Kata 13";
    document.body.appendChild(header);

    test13();
}
function test13() {

    bestThingArray = bestThing.slice(-12);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThing.slice(-12));
    document.body.appendChild(newElement)
    return bestThing.slice(-12); // Don't forget to return your output!
} // Don't forget to return your output!

function kata14() {
    let header = document.createElement("div");
    header.textContent = "Kata 14";
    document.body.appendChild(header);

    test14();
}
function test14() {

    bestThingArray = bestThing.slice(23, 38);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThing.slice(23, 38));
    document.body.appendChild(newElement)
    return bestThing.slice(23, 38); // Don't forget to return your output!
}
function kata15() {
    let header = document.createElement("div");
    header.textContent = "Kata 15";
    document.body.appendChild(header);

    test15();
}
function test15() {

    bestThingArray = bestThing.substring(-12);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThing.substring(bestThing.length-12));
    document.body.appendChild(newElement)
    return bestThing.substring(bestThing.length-12); // Don't forget to return your output!
} // Don't forget to return your output!
function kata16() {
    let header = document.createElement("div");
    header.textContent = "Kata 16";
    document.body.appendChild(header);

    test16();
}
function test16() {

    bestThingArray = bestThing.substring(23, 38);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(bestThing.substring(23, 38));
    document.body.appendChild(newElement)
    return bestThing.substring(23, 38); // Don't forget to return your output!
}
function kata17() {
    let header = document.createElement("div");
    header.textContent = "Kata17";
    document.body.appendChild(header);

    test17();
}
function test17() {
lotrCitiesArray.pop();
        

    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; // Don't forget to return your output!
}
function kata18() {
    let header = document.createElement("div");
    header.textContent = "Kata18";
    document.body.appendChild(header);

    test18();
}
function test18() {
  lotrCitiesArray.push("Deadest Marshes");        
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; // Don't forget to return your output!
}
function kata19() {
    let header = document.createElement("div");
    header.textContent = "Kata19";
    document.body.appendChild(header);

    test19();
}
function test19() {
            lotrCitiesArray.shift();
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; // Don't forget to return your output!
}
function kata20() {
    let header = document.createElement("div");
    header.textContent = "Kata20";
    document.body.appendChild(header);

    test20();
}
function test20() {
    
            lotrCitiesArray.unshift("Mordor");
    
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray; // Don't forget to return your output!
}